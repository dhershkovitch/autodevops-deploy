# GDK friendly deploy script

This repository contains a .gitlab-ci.yml file that has been modified to perform only the deploy stage. Further, it hardcodes a sample ruby image, as the GDK does not contain the registry.